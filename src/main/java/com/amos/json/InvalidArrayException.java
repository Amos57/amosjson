package com.amos.json;

public class InvalidArrayException extends RuntimeException {

    public InvalidArrayException(){
        super();
    }
  public    InvalidArrayException(String msg){
      super(msg);
  }
}
