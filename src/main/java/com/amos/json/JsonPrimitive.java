package com.amos.json;

public class JsonPrimitive extends JsonElement{

	
	private Object value;
	private boolean string;
	private boolean int_;
	private  boolean bool;
	private boolean double_;
	private boolean isNull;

	JsonPrimitive(){


	}

	@Override
	void setJson(String strval) {
		if(strval!=null)
			if("true".equals(strval)){
				bool=true;
				value=true;
			}else if("false".equals(strval)){
				bool=true;
				value=false;
			}else if("null".equals(strval)){
				isNull=true;
				value=null;
			}else if('\"'==strval.charAt(0)){
				string =true;
				//value=strval;
				value=JsonUtils.clearString(strval);
			}else{
				if(strval.contains(".")){
					double_=true;
					value = Double.parseDouble(strval);
				}else {
					int_ = true;
					value = Integer.parseInt(strval);
				}
			}
		else
			isNull=true;
	}

	public Object getValue(){
		return value;
	}

	void setJsonValue(Object js) {
       this.value=js;
	}
	
	public String asString(){
		return (String) value;
	}
	
	public int asInt(){
		return (Integer) value;
	}
	
	public boolean asBoolean(){
		return (Boolean) value;
	}
	
	public double asDouble(){
		return (Double) value;
	}


	public boolean isString(){
		return string;
	}

	public boolean isNull() {
		return isNull;
	}

	public boolean isDouble() {
		return double_;
	}

	public boolean isInt() {
		return int_;
	}

	public boolean isBoolean() {
		return bool;
	}

	public JsonPrimitive getAsJsonPrimitive(){
		return this;
	}

	@Override
	public boolean isPrimitive() {
		return true;
	}

	@Override
	public String toString() {
		if(value instanceof String){
			return "\""+value+"\"";
		}

		return String.valueOf(value);
	}
}