package com.amos.xml;

import java.io.IOException;
import java.io.InputStream;

import java.nio.charset.Charset;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class XMLParser {



    String document;
    Charset character;
    String versionDocument;


    static final String FIND_ATTRIBUTE_VALUE="=\"([^\"]*)\"";
    static final String FIND_TAGE="<[^>]+>|\n+";
    static final Pattern PATTERN_ATTRIBUTE = Pattern.compile(FIND_ATTRIBUTE_VALUE);
    static final Pattern PATTERN_TAG = Pattern.compile(FIND_TAGE);
    public XMLParser() {
        this.document = document;
    }


    public XMLElement parse(String xmlString)  {

        xmlString = removeComments(xmlString);
        XMLObject xml = new XMLObject();
        xml.setXML(xmlString);

        return xml;

    }

    public XMLElement parse(InputStream reader){

        StringBuilder sb = new StringBuilder();

        int i=0;


        try {
            while((i=reader.read())!=-1){
                sb.append((char)i);

            }

            reader.close();
        } catch (IOException e) {}

        String js = removeComments(sb.toString());
        XMLObject sJsonObject = new XMLObject();
        sJsonObject.setXML(js);

        return sJsonObject;

    }

    private String removeComments(String document){
int i=0;
        if(document.startsWith("<?xml")){
            int end=i=document.indexOf(">")+1;

            String data = document.substring("<?xml".length()+1,end);

            Matcher matcher=PATTERN_ATTRIBUTE.matcher(data);
int it=0;
            while (matcher.find()){
                int st=matcher.start();
                int ed=matcher.end();
                String pare=data.substring(it,st);
                int fSpace=pare.lastIndexOf(" ");
                String attrName=pare.substring(fSpace+1);
                if("version".equals(attrName))
                    versionDocument=data.substring(st+1,ed).replaceAll("\"","");
                else if("encoding".equals(attrName))
                    character= Charset.forName(data.substring(st+1,ed).replaceAll("\"",""));
               // System.out.println(attrName+"="+data.substring(st+1,ed).replaceAll("\"",""));

            }

        }

        StringBuilder stringBuilder= new StringBuilder(document.length());

        char[] array=document.toCharArray();



        for (; i < array.length; i++) {

            switch (array[i]){

                case '!':
                {
                    if(i==0)
                        throw new IllegalStateException("Invalid document");

                    if(array[i-1]=='<' && array[i+1]=='-' && array[i+2]=='-'){
                        i+=2;
                        String comment=document.substring(i);
                        int end=comment.indexOf("-->");
                        if(end==-1){
                            throw new IllegalStateException("Invalid document");
                        }
                        stringBuilder.delete(stringBuilder.length()-1,stringBuilder.length());
                        i+=end+2;

                        break;
                    }


                }
                default:
                    stringBuilder.append(array[i]);


            }
        }
        return stringBuilder.toString();


    }



}
