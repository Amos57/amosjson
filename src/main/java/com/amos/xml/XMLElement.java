package com.amos.xml;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Matcher;

public abstract class XMLElement {

    protected Map<String,XMLElement> node= new LinkedHashMap<>();
    protected Map<String,String> attributes = new LinkedHashMap<>();
    private String xmlDocument;



    protected void parser(String document){


    }



    public XMLObject getXmlObject(){
        return null;
    }


    public XMLArray getXmlArray(){
        return null;
    }


    public XMLPrimitive getXMXmlPrimitive(){
        return null;
    }


    public  void setXML(String xmlString){

            this.xmlDocument=xmlString;
            parse(xmlString);

    }

    protected String parseAttribute(String line) {
        Matcher matcher = XMLParser.PATTERN_ATTRIBUTE.matcher(line);
        int start=line.indexOf(" ");
        if(start==-1 || start==0){
            return line;
        }
        String res=line.substring(0,start);
        int it = 0;
        while (matcher.find()) {
            int st = matcher.start();
            int ed = matcher.end();
            String pare = line.substring(it, st);
            int fSpace = pare.lastIndexOf(" ");
            String attrName = pare.substring(fSpace + 1);
        }
        return res;
    }
        protected void parse (String xmlString){

            Matcher matcher = XMLParser.PATTERN_TAG.matcher(xmlString);

            while (matcher.find()) {

                String tags = xmlString.substring(matcher.start(), matcher.end()).replaceAll("\\r", "").replaceAll("\\n", "").replace("<", "").replace(">", "").trim();

                if ("".equals(tags)) continue;
                tags=parseAttribute(tags);
                int closeTag = xmlString.indexOf("</" + tags + ">");
                System.out.println(xmlString.substring(xmlString.indexOf(tags) + tags.length(), closeTag));

            }
        }


    }

