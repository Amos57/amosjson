package com.amos.json;

import java.lang.reflect.Array;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;


import com.amos.annotations.NotSerialization;
import com.amos.annotations.SerializationName;
import com.amos.json.*;

public class Json {

	public String toJson(Object obj) throws IllegalArgumentException, IllegalAccessException{
		if(obj==null)
		  throw new JsonNullPointerException("object is null");
		
		StringBuilder json = new StringBuilder("{");
		Class<?> cls = obj.getClass();
		Field[] filds = cls.getDeclaredFields();
		
		for(Field f : filds){
			
			NotSerialization nots = f.getAnnotation(NotSerialization.class);
			if(nots!=null || f.getModifiers()== Modifier.TRANSIENT)
				continue;
			json.append("\"");
			String name = f.getName();
			
            SerializationName sn = f.getAnnotation(SerializationName.class);
			
            if(sn!=null){
            	name = sn.name();
            }
            
	        json.append(name);
	        json.append("\":");
	        
	        Object val = getValueFromField(f,obj);
	        if(val == null){
	        	json.append("null");
	        }else if(val instanceof java.lang.Enum ){
	        	json.append("\"");
	        	json.append(val);
	        	json.append("\"");
	        }else
	        if(val instanceof String ){
	        	
	        	json.append("\"");
	        	json.append(val);
	        	json.append("\"");
	        }else if(val instanceof Character){
	        	json.append("\"");
	        	if((Character)val=='\u0000'){
	        		json.append(' ');
	        	}else
	        		json.append(val);
	        	json.append("\"");
	        	
	        	
	        }else if(val instanceof Number){
	        	json.append(val);
	        }else if(val instanceof Boolean){
	        	json.append(val);
	        }else if(val instanceof Number[]){
	        	json.append("[");	
	        	for(int i =0;i< ((Number[]) val).length;i++){
	        		json.append(((Number[])val)[i]);
	        		json.append(",");
	        	}
	        	json.delete(json.length()-1, json.length()).append("]");	
	        	
	        }else if(val instanceof int[]){
	        	json.append("[");	
	        	for(int i =0;i< ((int[]) val).length;i++){
	        		json.append(((int[])val)[i]);
	        		json.append(",");
	        	}
	        	json.delete(json.length()-1, json.length()).append("]");
	        }else if(val instanceof float[]){
	        	json.append("[");	
	        	for(int i =0;i< ((float[]) val).length;i++){
	        		json.append(((float[])val)[i]);
	        		json.append(",");
	        	}
	        	json.delete(json.length()-1, json.length()).append("]");
	        }else if(val instanceof byte[]){
	        	json.append("[");	
	        	for(int i =0;i< ((byte[]) val).length;i++){
	        		json.append(((byte[])val)[i]);
	        		json.append(",");
	        	}
	        	json.delete(json.length()-1, json.length()).append("]");
	        }else if(val instanceof long[]){
	        	json.append("[");	
	        	for(int i =0;i< ((long[]) val).length;i++){
	        		json.append(((long[])val)[i]);
	        		json.append(",");
	        	}
	        	json.delete(json.length()-1, json.length()).append("]");
	        }else if(val instanceof short[]){
	        	json.append("[");	
	        	for(int i =0;i< ((short[]) val).length;i++){
	        		json.append(((short[])val)[i]);
	        		json.append(",");
	        	}
	        	json.delete(json.length()-1, json.length()).append("]");
	        }else if(val instanceof boolean[]){
	        	json.append("[");	
	        	for(int i =0;i< ((boolean[]) val).length;i++){
	        		json.append(((boolean[])val)[i]);
	        		json.append(",");
	        	}
	        	json.delete(json.length()-1, json.length()).append("]");
	        }else if(val instanceof double[]){
	        	json.append("[");	
	        	for(int i =0;i< ((double[]) val).length;i++){
	        		json.append(((double[])val)[i]);
	        		json.append(",");
	        	}
	        	json.delete(json.length()-1, json.length()).append("]");
	        }else if(val instanceof char[]){
	        	json.append("[");	
	        	for(int i =0;i< ((char[]) val).length;i++){
	        		json.append(((char[])val)[i]);
	        		json.append(",");
	        	}
	        	json.delete(json.length()-1, json.length()).append("]");
	        }else if(val instanceof Character[]){
	        	json.append("[");	
	        	for(int i =0;i< ((Character[]) val).length;i++){
	        		json.append("\"");
	        		json.append(((Character[])val)[i]);
	        		json.append("\"");
	        		json.append(",");
	        	}
	        	json.delete(json.length()-1, json.length()).append("]");
	        }else if(val instanceof String[]){
	        	json.append("[");	
	        	for(int i =0;i< ((String[]) val).length;i++){
	        		json.append("\"");
	        		json.append(((String[])val)[i]);
	        		json.append("\"");
	        		json.append(",");
	        	}
	        	json.delete(json.length()-1, json.length()).append("]");
	        }else if(val instanceof Object[]){
	        	json.append("[");	
	        	for(int i =0;i< ((Object[]) val).length;i++){
	
	        		json.append(toJson(((Object[])val)[i]));
	
	        		json.append(",");
	        	}
	        	json.delete(json.length()-1, json.length()).append("]");
	        }else 
	        	json.append(toJson(val));
			
	        json.append(",");
		}
		if(json.length()==1)
			return json.append("}").toString();
		return json.delete(json.length()-1, json.length()).append("}").toString();
	}

	private Object getValueFromField(Field f,Object obj) throws IllegalArgumentException, IllegalAccessException {
		
		f.setAccessible(true);
		Object value = f.get(obj);
		f.setAccessible(false);
		

		return value;
	}	
		
	public Object fromJson(String json,Class<?> cls){
		if(json==null || cls==null)
			  throw new JsonNullPointerException("invalid parametrs");

		Object obj = null;
		try {
			//Constructor constructor=cls.getConstructor();
			obj = cls.newInstance();
		} catch (Exception e) {
			e.printStackTrace();
		    return null;
		}

		JsonParser jsonParser= new JsonParser();
        JsonObject jsonElement=jsonParser.saxParser(json).getAsJsonObject();


		for(Field f : cls.getDeclaredFields()){
			if(f.getModifiers()== Modifier.TRANSIENT)
		      continue;

			String name=f.getName();
			String type =f.getGenericType().getTypeName();
			JsonElement je=jsonElement.getElement(name);
			if(je==null)
				continue;

			if(je.isPrimitive()){
				JsonPrimitive jp=je.getAsJsonPrimitive();
				f.setAccessible(true);
				try {
					f.set(obj,jp.getValue());
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
				f.setAccessible(false);
			}else if(je.isObject()){
				JsonObject js=je.getAsJsonObject();
				try {
					f.setAccessible(true);
					f.set(obj,fromJson(js.toString(),f.getType()));
					f.setAccessible(false);
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
			}else if(je.isArray()){
				JsonArray ja=je.getAsJsonArray();
				Class typ=f.getType().getComponentType();
				Object array=  Array.newInstance(typ,ja.size());

				if(ja.isObjectType())
					for(int i=0;i<  ja.size();i++) {
						JsonObject jsonObject=ja.get(i).getAsJsonObject();
						Array.set(array, i,fromJson(jsonObject.toString(),typ));
					}
				else
					for(int i=0;i<  ja.size();i++) {
						JsonPrimitive jsonPrimitive=ja.get(i).getAsJsonPrimitive();
						Array.set(array, i,jsonPrimitive.getValue());
					}
				try {
					f.setAccessible(true);
					f.set(obj,array);

					f.setAccessible(false);
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
			}

		  }

		return obj;
		
		
	}
}
