package com.amos.json;

public final class JsonUtils {

    private JsonUtils() {
    }

    public static int endObject(String json) {

        int count = 0;
        int index = 0;
        for (char c : json.toCharArray()) {
            switch (c) {
                case '{':
                    count++;
                    break;
                case '}':
                    count--;
                    if (count == 0)
                        return index;


                    break;
            }
            index++;
        }

        return -1;
    }

    public static int endArray(String json) {

        int count = 0;
        int index = 0;
        for (char c : json.toCharArray()) {
            switch (c) {
                case '[':
                    count++;
                    break;
                case ']':
                    count--;
                    if (count == 0)
                        return index++;


                    break;
            }
            index++;
        }

        return -1;
    }


    public static int treeIndexOf(String txt, String el) {

        if (txt.length() < el.length()) {
            throw new IllegalArgumentException(" element more then json");
        }
         boolean res=true;
        char[] chArra = txt.toCharArray();
        for (int i = 1; i < chArra.length; i++) {
            int copyIterator = 0;
            res=true;
            if (txt.charAt(i) == '[') {
                i = endArray(new String(chArra, i, txt.length() - i))+i;
                continue;
            }
            if (txt.charAt(i) == '{') {
                i = endObject(new String(chArra, i, txt.length() - i))+i;
                continue;
            }

            if (txt.charAt(i) == el.charAt(0)) {

                copyIterator = i;
                int j;
                for (j = 0; j < el.length(); j++) {
                    if (txt.charAt(copyIterator) != el.charAt(j)) {
                       res=false;
                       break;
                    }
                        copyIterator++;
                }
                if (res) {
                    return i;

                }
            }

        }


        return -1;
    }

    public static Object cutPrimitiveValue(String json) {

        if (json.charAt(0) == '\"') {
            json = json.replaceFirst("\"", "");
            String value = json.substring(0, json.indexOf("\""));
            return value;
        } else if (json.startsWith("true")) {
            return true;
        } else if (json.startsWith("false")) {
            return false;
        } else {
            int i;
            String value = null;
            if ((i = json.indexOf(",")) != -1)
                value = json.substring(0, i);
            else
                value = json.substring(0, json.indexOf("}"));
            if (value.contains("."))
                return Double.parseDouble(value);
            else
                return Integer.parseInt(value);
        }
    }

    public static String clearString(String txt){
          txt=txt.replaceFirst("\"","");
          txt=txt.substring(0,txt.length()-1);
          return txt;
    }

    public static int endPrimitive(String element) {
        int index=element.indexOf(",");
        int	sc=element.indexOf("}");
        if(index==-1 && sc!=-1)
            index=sc;
        else if(index!=-1 && sc!=-1 && sc<index){
            index=sc;
        }

        return index;
    }
}