package com.amos.json;


import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;


public class JsonObject extends JsonElement implements Iterable<Map.Entry<String,JsonElement>>{

    private static final String FINDE_KEY="\"%s\":";
   // String json;

    private LinkedHashMap<String,JsonElement> linkedHashMap;
   public JsonObject(){

	   linkedHashMap= new LinkedHashMap<>();
   }


	public JsonObject getAsJsonObject(){
		return this;
	}
	public JsonElement get(String name){
		return node.get(name);
	}

	public JsonElement getElement(String name){
    	JsonElement jsonElement;
    	if((jsonElement=node.get(name))!=null)
    		return jsonElement;

        name=String.format(FINDE_KEY,name);
	    int keyLen = name.length();
		int i=JsonUtils.treeIndexOf(json,name);

		if(i==-1){
			return null;
		}
		int startVal =i + keyLen;

		String element = json.substring(startVal);
		if(element.charAt(0)=='{') {

			element=element.substring(0,JsonUtils.endObject(element)+1);
			jsonElement = new JsonObject();
			jsonElement.setJson(element);
			//jsonElement.object=true;
		}else if(element.charAt(0)=='['){
			element=element.substring(1,JsonUtils.endArray(element));
			jsonElement= new JsonArray();
			jsonElement.setJson(element);
		//	jsonElement.array=true;
		}else {
			element=element.substring(0,JsonUtils.endPrimitive(element));
			jsonElement= new JsonPrimitive();
			jsonElement.setJson(element);
			//jsonElement.primitive=true;
		}

		return jsonElement;

	}

	public void add(String property,JsonElement jsonElement){
   	    linkedHashMap.put(property,jsonElement);
	}
	public void addProperty(String property, Number value) {
		add(property, this.createJsonElement(value));
	}
	public void addProperty(String property, Boolean value) {
		add(property, this.createJsonElement(value));
	}
	public void addProperty(String property, Character value){
		add(property, this.createJsonElement(value));
	}
	public void addProperty(String property, String value){
		add(property, this.createJsonElement(value));
	}

	private JsonElement createJsonElement(String value){
		JsonPrimitive jsonElement=new JsonPrimitive();
		jsonElement.setJsonValue(value);
		return jsonElement;
	}
	private JsonElement createJsonElement(Boolean value){
		JsonPrimitive jsonElement=new JsonPrimitive();
		jsonElement.setJsonValue(value);
		return jsonElement;
	}
	private JsonElement createJsonElement(Character value){
		JsonPrimitive jsonElement=new JsonPrimitive();
		jsonElement.setJsonValue(value);
		return jsonElement;
	}
	private JsonElement createJsonElement(Number value){
		JsonPrimitive jsonElement=new JsonPrimitive();
		jsonElement.setJsonValue(value);
		return jsonElement;
	}

	@Override
	public boolean isObject() {
		return true;
	}

	public Iterator<Map.Entry<String,JsonElement>> iterator(){
		return node.entrySet().iterator();
	}

	@Override
	public String toString() {
   	if(json!=null)
   		return json;
	if(linkedHashMap.size()==0)
		return "{}";

	StringBuilder stringBuilder= new StringBuilder("{");
	for(Map.Entry<String,JsonElement> elementEntry:linkedHashMap.entrySet()){

	   stringBuilder.append("\""+elementEntry.getKey()+"\":");
		stringBuilder.append(elementEntry.getValue().toString()+",");

	}

		return stringBuilder.delete(stringBuilder.length()-1,stringBuilder.length()).append("}").toString();
	}
//public String getJson(){}
}
