package com.amos.json;


import java.io.IOException;
import java.io.InputStream;
import java.util.Stack;


public class JsonParser {

	
	public JsonElement parse(InputStream reader){
		
		StringBuilder sb = new StringBuilder();
		
		int i=0;
		
		
		try {
			while((i=reader.read())!=-1){
				sb.append((char)i);
				
			}
		} catch (IOException e) {}
		
		String js = removeCharacters(sb.toString());
		JsonObject sJsonObject = new JsonObject();
		sJsonObject.setJson(js);
		
	return sJsonObject;	
		
	}

	
	public JsonElement parse(String json)  {
		
		String js = removeCharacters(json);
		JsonElement sJsonObject = new JsonObject();
		sJsonObject.setJson(js);
		
	return sJsonObject;	
		
	}
	public JsonElement saxParser(String json)  {
        JsonElement sJsonObject = new JsonObject();
        sJsonObject.setJson(removeCharacters(json));
        return sJsonObject;
    }

	private String removeCharacters(String json)  {
		StringBuilder sb = new StringBuilder();

		boolean startValue=false;
		boolean befourValue=false;

		boolean string = false;

		boolean sc = false;
		char[] array=json.toCharArray();
		for(int i=0;i<array.length;i++ ){
			char c=array[i];
			switch (c) {
				case '\n':
					continue;
				case '\r':
					continue;
				case '\t':
					continue;
				case ' ':
					continue;
				case '{':
					startValue=false;
					befourValue=false;
					sb.append(c);
					continue;
				case '}':
					startValue=false;
					befourValue=false;
					sb.append(c);
					continue;
				case '\"':
					i=writeStringFromJson(json,i,sb);
					continue;
				case ':':
					befourValue=true;
					sb.append(c);
					continue;
				case ',':
					sb.append(c);
					if(sc)
						continue;
					befourValue=false;
					startValue=false;

					continue;
				default:
					sb.append(c);
			}


		}

		return sb.toString();

	}

	private int writeStringFromJson(String json,int offset,StringBuilder sb)  {


		for(;offset<json.length();offset++){
			char t=json.charAt(offset);

			sb.append(json.charAt(offset));
			if(t=='\"'){
				return offset++;
			}
		}

		throw new RuntimeException("invalid json");
	}



}
