package com.amos.json;


import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract  class JsonElement  {

	
	protected String json;
	//protected boolean array;
	//protected boolean object;
	protected boolean notFound;
	//protected boolean primitive;

    protected Map<String,JsonElement> node= new LinkedHashMap<String,JsonElement>();

    protected static final String TEST_REGEX="\"([^\"])\":";
    protected static final String KEY_REGEX="\"(\\w+)\":";


    JsonElement(){}
   // JsonElement(String json){

    //}


    protected void parse(String json){
    	if(json==null)
    		return;
		Pattern pattern=Pattern.compile(KEY_REGEX);
		Matcher matcher=pattern.matcher(json);
		int position = 0;
		while(matcher.find(position)){
			int start= matcher.start();
			int end  = matcher.end();
			String key = json.substring(start+1,end-2);
			String stValue=json.substring(end);
			JsonElement jsonElement = null;
			position=end;
			if('['==stValue.charAt(0)){
			//	array=true;

			//	object=false;
			//	primitive=false;
				int endArray=JsonUtils.endArray(stValue);
				String value=stValue.substring(1,endArray);
				jsonElement= new JsonArray();
				jsonElement.setJson(value);
				position=endArray;
			}else if('{'==stValue.charAt(0)){
			//	object=true;

			//	array=false;
			//	primitive=false;

				String value=stValue.substring(0,JsonUtils.endObject(stValue)+1);
				jsonElement= new JsonObject();
				jsonElement.setJson(value);
				int index=json.indexOf(value);
				position=index+value.length();

			}else{
			//primitive=true;

			//array=false;
			//object=false;
				int index=stValue.indexOf(",");
				int	sc=stValue.indexOf("}");
				if(index==-1 && sc!=-1)
					index=sc;
				else if(index!=-1 && sc!=-1 && sc<index){
					index=sc;
				}


				String value=stValue.substring(0,index);
				jsonElement= new JsonPrimitive();
				jsonElement.setJson(value);
				position=end;
			}
			node.put(key,jsonElement);

		}

	}

	public boolean isPrimitive() {
		return false;
	}

	public boolean isArray() {
		return false;
	}



	public boolean isObject() {
		return false;
	}


	public boolean isNotFound() {
		return notFound;
	}


	
/*	public JsonObject getAsJsonObject(){
		return null;
	}*/


    public JsonElement get(String name){
        return node.get(name);
    }

    public JsonObject getAsJsonObject(){
    return null;
    }
    public JsonArray getAsJsonArray(){
        return null;
    }

    public JsonPrimitive getAsJsonPrimitive(){
        return null;
    }




	 void setJson(String js){
		this.json=js;
		parse(json);
	}
	
	@Override
	public String toString() {
		return json;
	}
}