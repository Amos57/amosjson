package com.amos.json;

public class JsonNullPointerException extends RuntimeException{
	
	JsonNullPointerException(String msg) {
		super(msg);
	}

}
