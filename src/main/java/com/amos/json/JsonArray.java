package com.amos.json;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;



public class JsonArray extends JsonElement
		implements Iterable<JsonElement> {

	
	private List<JsonElement> elements = new ArrayList<JsonElement>();

    boolean string;
    boolean int_;
    boolean double_;
    boolean bool;
    boolean object;
    boolean array;
    boolean null_;

	JsonArray(){



	}

	@Override
	public JsonArray getAsJsonArray() {
		return this;
	}

	@Override
	void setJson(String js) {
		//super.setJson(js);
		if(js.isEmpty())
			return;
      char checkEl= js.charAt(0);
     
		if(checkEl=='\"'){
		    string=true;
		 String[] arr = js.split("\",\"");	
			for(String s : arr){
				s=s.replaceFirst("\"", "");
				s=replaceLast(s,"\"", "");
				
				JsonPrimitive jsonPrimitive = new JsonPrimitive();
				jsonPrimitive.setJsonValue(s);
				elements.add(jsonPrimitive);
			}
			
		}else if(checkEl=='{'){
			String[] arr = js.split("(?<=}),(?=\\{)");
			object=true;
			for(String s : arr) {
				JsonObject jsonObject = new JsonObject();
				jsonObject.setJson(s);
				elements.add(jsonObject);
			}
		}else if(checkEl=='[') {
            array = true;
            int startArray=1;
            int endArray;
            String tempArray=js;
            int pos=0;
            while((endArray=getEndArray(tempArray,pos))!=-1) {
                js=tempArray.substring(startArray,endArray);
                JsonArray jsonArray = new JsonArray();
                jsonArray.setJson(js);
                elements.add(jsonArray);
                pos=endArray+1;
                startArray=endArray+3;
            }
        }else if(js.startsWith("null")){
            null_=true;
            JsonPrimitive jsonPrimitive = new JsonPrimitive();
            jsonPrimitive.setJson(null);
            //value=null;
		}else if(js.startsWith("true") || js.startsWith("false")){
			String[] arr = js.split("\\,");
			bool=true;
			
			for(String s : arr){
				JsonPrimitive jsonPrimitive = new JsonPrimitive();
				jsonPrimitive.setJsonValue(s);
				jsonPrimitive.setJsonValue(Boolean.parseBoolean(s));
				elements.add(jsonPrimitive);

			}
		}else{
			
			String[] arr = js.split(",");
	
			for(String s : arr){
				JsonPrimitive jsonPrimitive = new JsonPrimitive();
				jsonPrimitive.setJson(s);
				if(s.contains(".")) {
                    jsonPrimitive.setJsonValue(Double.parseDouble(s));
                    double_=true;
				}else {
                    jsonPrimitive.setJsonValue(Integer.parseInt(s));
                    int_=true;
				}

				elements.add(jsonPrimitive);
			}
			
		}
		
	    }
	public JsonElement get(int index){
		return elements.get(index);
	}
	public int size(){
		return elements.size();
	}
	   public  String replaceLast(String text, String regex, String replacement) {
	        return text.replaceFirst("(?s)"+regex+"(?!.*?"+regex+")", replacement);
	    }

    @Override
    public boolean isArray() {
        return true;
    }

    public boolean isStringType(){
	    return string;
    }
    public boolean isObjectType(){
	    return object;
    }
    @Override
	public Iterator<JsonElement> iterator() {
		return elements.iterator();
	}


	private int getEndArray(String txt,int pos){
		Stack<Character> stack=new Stack<>();
		//stack.push('[');
		//if('['!=txt.charAt(0))
	//		throw new InvalidArrayException();
		for(int i=pos;i<txt.length();i++){
			if('['==txt.charAt(i)){
				stack.push('[');
			}else if(']'==txt.charAt(i)){
				stack.pop();
				if(stack.size()==0){
					return i;
				}
			}
		}
		return -1;
	}

	public void add(JsonElement js){
		elements.add(js);
	}
	@Override
	public String toString() {
		StringBuilder sb= new StringBuilder("[");
        for(JsonElement jsonElement:elements){
        	sb.append(jsonElement+",");
		}

		return sb.delete(sb.length()-1,sb.length()).append("]").toString();
	}
}
